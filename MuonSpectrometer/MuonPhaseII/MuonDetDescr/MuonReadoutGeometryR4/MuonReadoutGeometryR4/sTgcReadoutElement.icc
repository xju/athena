/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONREADOUTGEOMETRYR4_STGCREADOUTELEMENT_ICC
#define MUONREADOUTGEOMETRYR4_STGCREADOUTELEMENT_ICC


namespace MuonGMR4 {
    namespace sTgcIdMeasHashFields {
        constexpr unsigned int minusOne = -1;
        /// Hash field layout
        //// (wireInGrp)  | (channel)  [1-1024]  |  (sTgcChannelType) [0-2] | (gasGap -1) [1-4]
        constexpr unsigned int gasGapShift = 0;
        constexpr unsigned int chTypeShift = 3;        
        constexpr unsigned int wireInGrpBit = chTypeShift+ 2;
        constexpr unsigned int chanShift = wireInGrpBit + 1;        
        constexpr unsigned int wireInGrpShift = chanShift +9;
    }
    
inline double sTgcReadoutElement::chamberHeight() const { return 2.* m_pars.halfChamberHeight; }
inline double sTgcReadoutElement::sChamberLength() const { return 2.* m_pars.sHalfChamberLength; }
inline double sTgcReadoutElement::lChamberLength() const { return 2.* m_pars.lHalfChamberLength; }
inline double sTgcReadoutElement::sGapLength() const { return (m_pars.stripDesign ? 2.*m_pars.stripDesign->shortHalfHeight(): 0.); }
inline double sTgcReadoutElement::lGapLength() const { return (m_pars.stripDesign ? 2.*m_pars.stripDesign->longHalfHeight(): 0.); }
inline double sTgcReadoutElement::gapHeight() const { return (m_pars.stripDesign ? 2.*m_pars.stripDesign->halfWidth(): 0.); }
inline double sTgcReadoutElement::thickness() const { return 2.* m_pars.halfChamberTck; }
inline double sTgcReadoutElement::sFrameWidth() const { return m_pars.sFrameWidth; }
inline double sTgcReadoutElement::lFrameWidth() const { return m_pars.lFrameWidth; }
inline int sTgcReadoutElement::multilayer() const { return m_multiLayer; }
inline int sTgcReadoutElement::numLayers() const { return m_pars.numLayers; }
inline unsigned int sTgcReadoutElement::nChTypes() const { return m_pars.nChTypes; }
inline double sTgcReadoutElement::gasGapThickness() const { return m_pars.gasTck; }
inline double sTgcReadoutElement::gasGapPitch() const { return m_gasGapPitch; }
inline double sTgcReadoutElement::yCutout() const { return m_pars.yCutout; }
inline unsigned int sTgcReadoutElement::numStrips() const { return (m_pars.stripDesign ? m_pars.stripDesign->numStrips(): 0u); }
inline double sTgcReadoutElement::stripPitch() const { return (m_pars.stripDesign ? m_pars.stripDesign->stripPitch(): 0.); }
inline double sTgcReadoutElement::stripWidth() const { return (m_pars.stripDesign ? m_pars.stripDesign->stripWidth(): 0.); }
inline double sTgcReadoutElement::stripLength(const int& stripNumb) const { return (m_pars.stripDesign ? m_pars.stripDesign->stripLength(stripNumb): 0.); }
inline IdentifierHash sTgcReadoutElement::measurementHash(const Identifier& measId) const {
    if (idHelperSvc()->detElId(measId) != identify()) {
        ATH_MSG_WARNING("The measurement " << idHelperSvc()->toString(measId)
                        << " picks the wrong readout element " << idHelperSvc()->toStringDetEl(identify()));
    }
    return createHash(m_idHelper.gasGap(measId),
                      m_idHelper.channelType(measId),
                      m_idHelper.channel(measId));
}
inline IdentifierHash sTgcReadoutElement::createHash(const unsigned int gasGap, 
                                                     const unsigned int channelType, 
                                                     const unsigned int channel,
                                                     const unsigned int wireInGrp) {
    using namespace sTgcIdMeasHashFields;
    /// Construct the Hash such that  (channel) | (channelType) | (gasGap -1) 
    if (channelType == ReadoutChannelType::WireInGrp) {
        const unsigned int readOutHash = static_cast<unsigned int>(createHash(gasGap, ReadoutChannelType::Wire, channel));
        return IdentifierHash {wireInGrp << wireInGrpShift |  wireInGrpBit | readOutHash};
    }
    return IdentifierHash{ channel << chanShift | channelType << chTypeShift | (gasGap -1) };
}

inline IdentifierHash sTgcReadoutElement::layerHash(const IdentifierHash& measHash) {
    using namespace sTgcIdMeasHashFields;
    constexpr unsigned int mask = (minusOne << chanShift) | (1<<wireInGrpBit);
    return IdentifierHash{static_cast<unsigned int>(measHash) & (~mask)};
}

inline  unsigned int sTgcReadoutElement::stripNumber(const IdentifierHash& measHash) {
    using namespace sTgcIdMeasHashFields;
    constexpr unsigned int mask = (minusOne << wireInGrpShift);
    const unsigned int stripedHash = (~mask) & static_cast<unsigned int>(measHash);
    return stripedHash >> chanShift;
}
inline unsigned int sTgcReadoutElement::chType(const IdentifierHash& measHash) {
    using namespace sTgcIdMeasHashFields;
    constexpr unsigned int mask = (minusOne << chanShift);
    const unsigned int stripedHash = (~mask) & static_cast<unsigned int>(measHash);
    return stripedHash >> chTypeShift;
    
}
inline unsigned int sTgcReadoutElement::gasGapNumber(const IdentifierHash& measHash) {
    using namespace sTgcIdMeasHashFields;
    constexpr unsigned int mask = (minusOne << chTypeShift);
    return (static_cast<unsigned int>(measHash) &(~mask) );
}
inline Identifier sTgcReadoutElement::measurementId(const IdentifierHash& measHash) const {
    return m_idHelper.channelID(identify(), multilayer(), gasGapNumber(measHash) + 1, chType(measHash), stripNumber(measHash));  
}
inline IdentifierHash sTgcReadoutElement::layerHash(const Identifier& measId) const {
    if (m_idHelper.elementID(measId) != m_idHelper.elementID(identify()) ) {
        ATH_MSG_WARNING("The measurement " << idHelperSvc()->toString(measId)
                        << " picks the wrong readout element " << idHelperSvc()->toStringDetEl(identify()));
    }
    return createHash(m_idHelper.gasGap(measId), m_idHelper.channelType(measId), 0);
}


 inline Amg::Vector3D sTgcReadoutElement::stripPosition(const ActsGeometryContext& ctx, const Identifier& measId) const {
        return stripPosition(ctx, measurementHash(measId));
 }
 inline const StripLayer& sTgcReadoutElement::stripLayer(const IdentifierHash& measHash) const {
    unsigned int gasGap = gasGapNumber(measHash);
    return m_pars.stripLayers[gasGap];
 }
 inline const StripLayer& sTgcReadoutElement::stripLayer(const Identifier& measId) const {
    return stripLayer(measurementHash(measId));
    //return m_pars.stripLayers[m_idHelper.gasGap(measId)-1];
 }

}  // namespace MuonGMR4
#endif
